import 'package:demo_work/screens/account_screen.dart';
import 'package:demo_work/screens/history_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:demo_work/screens/enquiry_screen.dart';
import 'package:demo_work/utils/colors.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: _buildTheme(Brightness.light),
      home: const Scaffold(
        body: MyHomePage(),
      ),
    );
  }

  ThemeData _buildTheme(brightness) {
    var baseTheme = ThemeData(brightness: brightness);

    return baseTheme.copyWith(
      textTheme: GoogleFonts.interTextTheme(baseTheme.textTheme),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  final tabs = <Widget>[
    const EnquiryScreen(),
    const HistoryScreen(),
    const AccountScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: tabs[_selectedIndex]),
      backgroundColor: AppColors.bgColor,
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Image.asset(
                'assets/icons/bottom_navigation_bar/enquiry_icon.png'),
            activeIcon: Image.asset(
                'assets/icons/bottom_navigation_bar/enquiry_icon_selected.png'),
            label: 'Enquiry',
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
                'assets/icons/bottom_navigation_bar/history_icon.png'),
            activeIcon: Image.asset(
                'assets/icons/bottom_navigation_bar/history_icon_selected.png'),
            label: 'History',
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
                'assets/icons/bottom_navigation_bar/account_icon.png'),
            activeIcon: Image.asset(
                'assets/icons/bottom_navigation_bar/account_icon_selected.png'),
            label: 'Account',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: AppColors.primaryColor,
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
