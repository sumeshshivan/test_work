import 'package:demo_work/screens/enquiry_screen.dart';
import 'package:demo_work/utils/colors.dart';
import 'package:flutter/material.dart';

class HistoryScreen extends StatefulWidget {
  const HistoryScreen({super.key});

  @override
  State<HistoryScreen> createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          toolbarHeight: 20,
          elevation: 1,
          shadowColor: const Color(0xFFEBEBEB),
          bottom: const TabBar(
            // isScrollable: true,
            padding: EdgeInsets.symmetric(horizontal: 36),
            // tabAlignment: TabAlignment.start,
            labelColor: AppColors.primaryColor,
            indicatorColor: AppColors.primaryColor,
            unselectedLabelColor: AppColors.secondaryTextColor,
            tabs: [
              Tab(
                text: "Active Orders",
              ),
              Tab(
                text: "Past Orders",
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            Container(
              color: AppColors.bgColor,
              width: double.infinity,
              child: Column(children: [Expanded(
                  child: ListView.separated(
                      padding: const EdgeInsets.only(top: 5),
                      itemBuilder: (context, index) => const HistoryCard(),
                      separatorBuilder: (context, index) => const SizedBox(
                            height: 5,
                          ),
                      itemCount: 5)),]
              )),
            Container(
              color: AppColors.bgColor,
              width: double.infinity,
            )
          ],
        ),
      ),
    );
  }
}

class HistoryCard extends StatelessWidget {
  const HistoryCard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 16.0, horizontal: 26.0),
                child: SizedBox(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          const Text("ABCD 602452369",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: AppColors.textColor)),
                          const SizedBox(
                            width: 45,
                          ),
                          SizedBox(
                            height: 14,
                            child: Image.asset('assets/icons/green_tick.png'),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Container(
                        height: 50,
                        child: const Text(
                            "Order delivered on 21 Aug 2022, 12:52 PM by Althaf",
                            softWrap: true,
                            style: TextStyle(
                                fontSize: 14,
                                height: 26 / 14,
                                color: AppColors.secondaryTextColor)),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: ClipRRect(
                  child: Container(
                      width: double.infinity,
                      height: 1,
                      child: CustomPaint(painter: DashedLinePainter())),
                ),
              ),
              const Padding(
                padding:
                  EdgeInsets.symmetric(vertical: 14, horizontal: 26),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Online payment",
                        style:
                            TextStyle(fontSize: 15, color: Color(0xFF5733A5))),
                    Text("₹48",
                        style: TextStyle(
                            fontSize: 14,
                            color: AppColors.textColor,
                            fontWeight: FontWeight.w600)),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
