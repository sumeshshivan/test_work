import 'package:demo_work/screens/enquiry_screen.dart';
import 'package:demo_work/utils/colors.dart';
import 'package:flutter/material.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({super.key});

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 1,
          toolbarHeight: 80,
          shadowColor: const Color(0xFFEBEBEB),
          title: Row(children: [
            SizedBox(
                height: 43,
                child: Image.asset(
                    'assets/icons/profile_placeholder_rounded.png')),
            const SizedBox(
              width: 20,
            ),
            const Text(
              "Althaf Rahman",
              style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w700,
                  color: AppColors.textColor),
            ),
            const Spacer(),
            const Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "View more",
                  style: TextStyle(fontSize: 14, color: AppColors.redColor),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 3.0),
                  child: Icon(
                    Icons.arrow_right,
                    color: AppColors.redColor,
                    size: 20,
                  ),
                ),
              ],
            ),
          ]),
        ),
        body: Container(
            color: AppColors.bgColor,
            width: double.infinity,
            child: ListView(
              padding: const EdgeInsets.symmetric(vertical: 6),
              children: <Widget>[
                SizedBox(
                  height: 75,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Expanded(
                          child: Container(
                              // padding: const EdgeInsets.only(left: 20),
                              color: Colors.white,
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                        height: 23,
                                        child: Image.asset(
                                            'assets/icons/call.png')),
                                    const SizedBox(
                                      width: 15,
                                    ),
                                    const Text(
                                      "Help & Support",
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: AppColors.textColor),
                                    ),
                                  ]))),
                      const SizedBox(
                        width: 6,
                      ),
                      Expanded(
                          child: Container(
                              padding: const EdgeInsets.all(26),
                              color: Colors.white,
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                        height: 23,
                                        child: Image.asset(
                                            'assets/icons/settings.png')),
                                    const SizedBox(
                                      width: 15,
                                    ),
                                    const Text(
                                      "settings",
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: AppColors.textColor),
                                    ),
                                  ]))),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 6,
                ),
                const OrderSettingsSections(),
                const SizedBox(
                  height: 6,
                ),
                const PaymentsSettingsSections(),
              ],
            )));
  }
}

class PaymentsSettingsSections extends StatelessWidget {
  const PaymentsSettingsSections({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(14),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 4.0),
            child: Text('Payments & coupons.',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: AppColors.textColor)),
          ),
          const Divider(),
          Padding(
            padding:
                const EdgeInsets.only(left: 14, top: 18, right: 14, bottom: 14),
            child: Row(children: [
              SizedBox(
                  height: 16,
                  child: Image.asset('assets/icons/payment_settings.png')),
              const SizedBox(
                width: 20,
              ),
              const Text(
                "Payments & refund",
                style:
                    TextStyle(fontSize: 15, color: AppColors.textMediumColor),
              ),
              const Spacer(),
              const Padding(
                padding: EdgeInsets.only(top: 3.0),
                child: Icon(
                  Icons.chevron_right,
                  color: AppColors.textColor,
                  size: 20,
                ),
              ),
            ]),
          ),
          Padding(
            padding: const EdgeInsets.all(14),
            child: Row(children: [
              SizedBox(
                  width: 16,
                  child: Image.asset('assets/icons/offer_settings.png')),
              const SizedBox(
                width: 25,
              ),
              const Text(
                "Explore offers",
                style:
                    TextStyle(fontSize: 15, color: AppColors.textMediumColor),
              ),
              const Spacer(),
              const Padding(
                padding: EdgeInsets.only(top: 3.0),
                child: Icon(
                  Icons.chevron_right,
                  color: AppColors.textColor,
                  size: 20,
                ),
              ),
            ]),
          ),
          Padding(
            padding: const EdgeInsets.all(14),
            child: Row(children: [
              SizedBox(
                  height: 19,
                  child: Image.asset('assets/icons/share_settings.png')),
              const SizedBox(
                width: 24,
              ),
              const Text(
                "Refers & earn ",
                style:
                    TextStyle(fontSize: 15, color: AppColors.textMediumColor),
              ),
              const Spacer(),
              const Padding(
                padding: EdgeInsets.only(top: 3.0),
                child: Icon(
                  Icons.chevron_right,
                  color: AppColors.textColor,
                  size: 20,
                ),
              ),
            ]),
          )
        ],
      ),
    );
  }
}

class OrderSettingsSections extends StatelessWidget {
  const OrderSettingsSections({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(14),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 4.0),
            child: Text('Order settings.',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: AppColors.textColor)),
          ),
          const Divider(),
          Padding(
            padding:
                const EdgeInsets.only(left: 14, top: 18, right: 14, bottom: 14),
            child: Row(children: [
              SizedBox(
                  height: 21,
                  child: Image.asset('assets/icons/oder_history_settings.png')),
              const SizedBox(
                width: 23,
              ),
              const Text(
                "Orders History",
                style:
                    TextStyle(fontSize: 15, color: AppColors.textMediumColor),
              ),
              const Spacer(),
              const Padding(
                padding: EdgeInsets.only(top: 3.0),
                child: Icon(
                  Icons.chevron_right,
                  color: AppColors.textColor,
                  size: 20,
                ),
              ),
            ]),
          ),
          Padding(
            padding: const EdgeInsets.all(14),
            child: Row(children: [
              SizedBox(
                  height: 18,
                  child: Image.asset('assets/icons/address_settings.png')),
              const SizedBox(
                width: 23,
              ),
              const Text(
                "Address details",
                style:
                    TextStyle(fontSize: 15, color: AppColors.textMediumColor),
              ),
              const Spacer(),
              const Padding(
                padding: EdgeInsets.only(top: 3.0),
                child: Icon(
                  Icons.chevron_right,
                  color: AppColors.textColor,
                  size: 20,
                ),
              ),
            ]),
          )
        ],
      ),
    );
  }
}
