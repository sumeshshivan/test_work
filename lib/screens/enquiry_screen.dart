import 'package:demo_work/utils/colors.dart';
import 'package:flutter/material.dart';

class EnquiryScreen extends StatefulWidget {
  const EnquiryScreen({super.key});

  @override
  State<EnquiryScreen> createState() => _EnquiryScreenState();
}

class _EnquiryScreenState extends State<EnquiryScreen> {
  bool stroreStatus = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: NestedScrollView(
            headerSliverBuilder: (conext, innerBoxScrolled) => [
                  SliverAppBar(
                    backgroundColor: AppColors.primaryColor,
                    expandedHeight: 200.0,
                    flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Spacer(),
                          const Text(
                            "Althaf Rahman",
                            style: TextStyle(fontSize: 17),
                          ),
                          // const SizedBox(
                          //   height: 8,
                          // ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const Text("Store Status",
                                  style: TextStyle(fontSize: 12)),
                              Transform.scale(
                                scale: 0.6,
                                child: Switch(
                                  value: stroreStatus,
                                  inactiveTrackColor: Colors.white,
                                  onChanged: (bool value) {
                                    setState(() {
                                      stroreStatus = value;
                                    });
                                  },
                                ),
                              ),
                              SizedBox(
                                width: 50,
                                child: stroreStatus
                                    ? const Text("Open",
                                        style: TextStyle(fontSize: 12))
                                    : const Text("Closed",
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: AppColors.redColor)),
                              )
                            ],
                          )
                        ],
                      )
                  ),
                  )
                ],
            body: Container(
              padding: const EdgeInsets.only(
                left: 12,
                top: 36,
                right: 12,
              ),
              color: AppColors.bgColor,
              child: Column(
                children: [
                  const Row(
                    children: [
                      Text("Call History",
                          style: TextStyle(
                              fontSize: 17, color: AppColors.textColor)),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Expanded(
                      child: ListView.separated(
                          padding: EdgeInsets.zero,
                          itemBuilder: (context, index) => const EnquiryCard(),
                          separatorBuilder: (context, index) => const SizedBox(
                                height: 12,
                              ),
                          itemCount: 3)),
                ],
              ),
            )));
  }
}

class EnquiryCard extends StatelessWidget {
  const EnquiryCard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(11)),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 16.0, horizontal: 12.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 32,
                      child:
                          Image.asset('assets/icons/profile_placeholder.png'),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("Althaf Rahman",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: AppColors.textColor)),
                        const SizedBox(
                          height: 4,
                        ),
                        Container(
                          height: 50,
                          child: const Text(
                              "4517 Washington Ave. Manchester,\nKentucky 39495",
                              softWrap: true,
                              style: TextStyle(
                                  fontSize: 14,
                                  height: 26 / 14,
                                  color: AppColors.secondaryTextColor)),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: ClipRRect(
                  child: Container(
                      width: double.infinity,
                      height: 1,
                      child: CustomPaint(painter: DashedLinePainter())),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 14, horizontal: 24),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("12 Jun 23",
                        style: TextStyle(
                            fontSize: 14, color: AppColors.secondaryTextColor)),
                    const Text("12.5 Km",
                        style: TextStyle(
                            fontSize: 14, color: AppColors.secondaryTextColor)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 13,
                          child: Image.asset('assets/icons/unmarked.png'),
                        ),
                        const SizedBox(
                          width: 9,
                        ),
                        const Text("Unmarked",
                            style: TextStyle(
                                fontSize: 14,
                                color: AppColors.secondaryTextColor)),
                      ],
                    )
                  ],
                ),
              ),
              InkWell(
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(0xFFEBE9FF),
                      borderRadius: BorderRadius.circular(8)),
                  padding: EdgeInsets.all(16),
                  width: double.infinity,
                  child: const Center(
                      child: Text(
                    "View Order",
                    style:
                        TextStyle(color: AppColors.primaryColor, fontSize: 16),
                  )),
                ),
              )
            ],
          ),
        ));
  }
}

class DashedLinePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    double dashWidth = 9, dashSpace = 5, startX = 0;
    final paint = Paint()
      ..color = const Color(0xFFDCDCDC)
      ..strokeWidth = 1;
    while (startX < size.width) {
      canvas.drawLine(Offset(startX, 0), Offset(startX + dashWidth, 0), paint);
      startX += dashWidth + dashSpace;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
