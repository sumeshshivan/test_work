import 'dart:ui';

class AppColors {
  static const primaryColor = Color(0xFF08005F);
  static const bgColor = Color(0xFFF7F6FF);
  static const redColor = Color(0xFFFF3838);
  static const textColor = Color(0xFF454545);
  static const secondaryTextColor = Color(0xFF979797);
  static const textMediumColor = Color(0xFF5B5B5B);
}
